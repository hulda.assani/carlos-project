exports.SUPORTED_EVENT_COLORS = [
  "#000000",
  "#F00000",
  "#F09E00",
  "#F0E600",
  "#00F018",
  "#979797",
  "#7B2525",
  "#EB00F0",
  "#7800F0",
  "#008BF0",
];

exports.AUTHENTICATED_SESSION_NAME = "auth";