export default (context, inject) => {
  inject('getPage', (currentPage, perPage, totalElements) => {
    perPage = perPage === -1 ? totalElements : perPage
    const page = {
      perPage: perPage,
      totalElements: totalElements,
      currentPage: currentPage,
      hasPreviousPage: currentPage > 1,
      previousPage: currentPage - 1,
      hasNextPage: perPage * currentPage < totalElements,
      nextPage: currentPage + 1,
      totalPages: Math.abs(Math.ceil(totalElements / perPage)),
    }
    return page
  })
}
