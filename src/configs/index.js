const EVENT_MANAGER_SERVER_URL = "http://localhost:3120";
const EVENT_MANAGER_LOCAL_SERVER_URL = "http://localhost:3120";

const EVENT_MANAGER_APP_BASE_URL = "http://localhost:8081";
const EVENT_MANAGER_LOCAL_APP_BASE_URL = "http://localhost:8081";

const inProduction = process.env.NODE_ENV === "production";

const API_ADMINS_ENDPOINT = "api/admins";

exports.API_V1_URL_AUTH_ENDPOINT = inProduction
  ? `${EVENT_MANAGER_SERVER_URL}/${API_ADMINS_ENDPOINT}/v1/auth`
  : `${EVENT_MANAGER_LOCAL_SERVER_URL}/${API_ADMINS_ENDPOINT}/v1/auth`;

exports.API_V1_URL_ADMINS_ENDPOINT = inProduction
  ? `${EVENT_MANAGER_SERVER_URL}/${API_ADMINS_ENDPOINT}/v1/admins`
  : `${EVENT_MANAGER_LOCAL_SERVER_URL}/${API_ADMINS_ENDPOINT}/v1/admins`;

exports.API_V1_URL_EVENTS_ENDPOINT = inProduction
  ? `${EVENT_MANAGER_SERVER_URL}/api/v1/events`
  : `${EVENT_MANAGER_LOCAL_SERVER_URL}/api/v1/events`;

exports.API_V1_URL_MEDIA_ENDPOINT = inProduction
  ? `${EVENT_MANAGER_SERVER_URL}/api/v1/media-files`
  : `${EVENT_MANAGER_LOCAL_SERVER_URL}/api/v1/media-files`;

exports.EVENT_MANAGER_SERVER_URL = inProduction
  ? EVENT_MANAGER_SERVER_URL
  : EVENT_MANAGER_LOCAL_SERVER_URL;

exports.EVENT_MANAGER_APP_BASE_URL = inProduction
  ? EVENT_MANAGER_APP_BASE_URL
  : EVENT_MANAGER_LOCAL_APP_BASE_URL;
