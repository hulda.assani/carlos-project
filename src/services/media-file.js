import { API_V1_URL_MEDIA_ENDPOINT } from "../configs";

function MediaFileService(axios) {
  const getDefaultIcons = () => {
    return axios({
      method: "GET",
      url: `${API_V1_URL_MEDIA_ENDPOINT}/default-icons`,
    })
  };

  const createMediaFile = (data) => {
    return axios({
      method: "POST",
      url: `${API_V1_URL_MEDIA_ENDPOINT}`,
      data,
    })
  };

  return {
    getDefaultIcons,
    createMediaFile,
  };
}

export default MediaFileService;
