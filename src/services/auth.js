import jwt_decode from "jwt-decode";

import { API_V1_URL_AUTH_ENDPOINT } from "../configs";
import { AUTHENTICATED_SESSION_NAME } from "../configs/constants";

function AuthService(axios) {
  const setToken = (token) => {
    var decodedToken = jwt_decode(token);
    const item = {
      token: token,
      expiry: decodedToken.exp * 1000,
    };
    localStorage.setItem(AUTHENTICATED_SESSION_NAME, JSON.stringify(item));
  };

  const getToken = () => {
    const authenticated = localStorage.getItem(AUTHENTICATED_SESSION_NAME);
    if (!authenticated) return null;
    const item = JSON.parse(authenticated);
    const now = new Date();
    if (now.getTime() > item.expiry) {
      logout();
      return null;
    }
    return item;
  };

  const logout = () => {
    localStorage.removeItem(AUTHENTICATED_SESSION_NAME);
  };

  const login = (data) => {
    return new Promise((resolve, reject) => {
      axios({
        method: "POST",
        url: `${API_V1_URL_AUTH_ENDPOINT}/login`,
        data,
      })
        .then((response) => {
          setToken(response.data.token);
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  };

  const resetPassword = (data) => {
    return axios({
      method: "POST",
      url: `${API_V1_URL_AUTH_ENDPOINT}/reset-password`,
      data: data,
    });
  };

  const sendPasswordResetMail = (data) => {
    return axios({
      method: "POST",
      url: `${API_V1_URL_AUTH_ENDPOINT}/send-password-reset-email`,
      data: data,
    });
  };

  return {
    getToken,
    logout,
    login,
    resetPassword,
    sendPasswordResetMail,
  };
}

export default AuthService;
