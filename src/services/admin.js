import { API_V1_URL_ADMINS_ENDPOINT } from "../configs";

function AdminService(axios) {
  const getCurrentAdmin = () => {
    return axios({
      method: "GET",
      url: `${API_V1_URL_ADMINS_ENDPOINT}/current`,
    });
  };

  const fetchAdminByResetToken = (token) => {
    return axios({
      method: "GET",
      url: `${API_V1_URL_ADMINS_ENDPOINT}/get-by-reset-token/${token}`,
    });
  };

  const updateAdmin = (data) => {
    return axios({
      method: "PUT",
      url: `${API_V1_URL_ADMINS_ENDPOINT}/current`,
      data,
    });
  };

  const updateAdminPassword = (data) => {
    return axios({
      method: "PUT",
      url: `${API_V1_URL_ADMINS_ENDPOINT}/current/update-password`,
      data,
    });
  };

  const updateAdminPhoto = (data) => {
    return axios({
      method: "PUT",
      url: `${API_V1_URL_ADMINS_ENDPOINT}/current/update-photo`,
      data,
    });
  };

  return {
    getCurrentAdmin,
    fetchAdminByResetToken,
    updateAdmin,
    updateAdminPassword,
    updateAdminPhoto,
  };
}

export default AdminService;
