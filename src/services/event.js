import { API_V1_URL_EVENTS_ENDPOINT } from "../configs";

function EventService(axios) {
  const getEvents = () => {
    return axios({
      method: "GET",
      url: `${API_V1_URL_EVENTS_ENDPOINT}`,
    });
  };

  const getEvent = (id) => {
    return axios({
      method: "GET",
      url: `${API_V1_URL_EVENTS_ENDPOINT}/${id}`,
    });
  };

  const createEvent = (data) => {
    return axios({
      method: "POST",
      url: `${API_V1_URL_EVENTS_ENDPOINT}`,
      data,
    })
  };

  const deleteEvent = (id) => {
    return axios({
      method: "DELETE",
      url: `${API_V1_URL_EVENTS_ENDPOINT}/${id}`,
    })
  };

  return {
    getEvents,
    getEvent,
    createEvent,
    deleteEvent,
  };
}

export default EventService;
