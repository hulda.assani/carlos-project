import { createWebHistory, createRouter } from "vue-router";

import AuthService from "../services/auth";

/**
 * Layouts
 */

import Auth from "@/layouts/Auth.vue";
import Dashboard from "@/layouts/Dashboard.vue";

/**
 * Views for Auth layout
 */

import Login from "@/pages/auth/Login.vue";
import ForgotPassword from "@/pages/auth/ForgotPassword.vue";
import ResetPassword from "@/pages/auth/ResetPassword.vue";

/**
 * Views for Dashboard layout
 */

import DashboardHome from "@/pages/dashboard/Home.vue";

/**
 * Routes
 */

const routes = [
  {
    path: "/auth",
    redirect: "/auth/login",
    component: Auth,
    children: [
      {
        path: "/auth/login",
        component: Login,
        name: "Login",
      },
      {
        path: "/auth/forgot-password",
        component: ForgotPassword,
        name: "ForgotPassword",
      },
      {
        path: "/auth/reset-password",
        component: ResetPassword,
        name: "ResetPassword",
        beforeEnter(to, from, next) {
          if (!to.query.token || to.query.token.length === 0) {
            to.query.hasToken = false;
            next();
          } else {
            to.query.hasToken = true;
            next();
          }
        },
      },
    ],
  },
  {
    path: "/app",
    redirect: "/app/dashboard",
    component: Dashboard,
    meta: {
      requiresAuth: true,
    },
    children: [
      {
        path: "/app/dashboard",
        component: DashboardHome,
        name: "DashboardHome",
      },
    ],
  },
  { path: "/:pathMatch(.*)*", redirect: "/auth" },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

/**
 * Auth guard
 */

router.beforeEach((to, from, next) => {
  if (to.matched.some((rec) => rec.meta.requiresAuth)) {
    const data = AuthService(null, null).getToken();
    if (data) {
      const now = new Date();
      if (now.getTime() > data.expiry) {
        AuthService(null, null).logout();
        next({ name: "Login" });
        return;
      }
      next();
    } else {
      next({ name: "Login" });
    }
  } else {
    next();
  }
});

export default router;
