export const setCurrentAdmin = (state, admin) => {
    state.currentAdmin = admin;
};

export const updateCurrentAdmin = (state, admin) => {
    if (admin.email) {
        state.currentAdmin.email = admin.email;
    }
    if (admin.photo) {
        state.currentAdmin.photo = admin.photo;
    }
};
