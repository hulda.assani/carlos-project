export async function fetchCurrentAdmin({ dispatch }, services) {
  try {
    const response = await services.AdminService.getCurrentAdmin();
    dispatch("setCurrentAdmin", response.data);
    return Promise.resolve(true);
  } catch (error) {
    return Promise.reject(error);
  }
}

export async function fetchAdminByResetToken(context, { token, services }) {
  try {
    const response = await services.AdminService.fetchAdminByResetToken(token);
    return Promise.resolve(response.data);
  } catch (error) {
    return Promise.reject(error);
  }
}

export async function updateAdminEmail({ dispatch }, { data, services }) {
  try {
    const response = await services.AdminService.updateAdmin(data);
    dispatch("updateCurrentAdmin", response.data);
    return Promise.resolve(true);
  } catch (error) {
    return Promise.reject(error);
  }
}

export async function updateAdminPassword(context, { data, services }) {
  try {
    const response = await services.AdminService.updateAdminPassword(data);
    return Promise.resolve(response.data);
  } catch (error) {
    return Promise.reject(error);
  }
}

export async function updateAdminPhoto({ dispatch }, { data, services }) {
  try {
    const response = await services.AdminService.updateAdminPhoto(data);
    dispatch("updateCurrentAdmin", response.data);
  } catch (error) {
    return Promise.reject(error);
  }
}

export const setCurrentAdmin = ({ commit }, payload) => {
  commit("setCurrentAdmin", payload);
};

export const updateCurrentAdmin = ({ commit }, payload) => {
  commit("updateCurrentAdmin", payload);
};
