export async function fetchEvents({ dispatch }, services) {
  try {
    const response = await services.EventService.getEvents();
    const data = response.data.data;
    const pagination = {
      perPage: data.perPage,
      totalElements: data.totalElements,
      currentPage: data.currentPage,
      hasPreviousPage: data.hasPreviousPage,
      previousPage: data.previousPage,
      hasNextPage: data.hasNextPage,
      nextPage: data.nextPage,
      totalPages: data.totalPages,
    };
    dispatch("setEvents", { events: data.events, pagination });
    return Promise.resolve(true);
  } catch (error) {
    return Promise.reject(error);
  }
}

export async function fetchEvent(context, { id, services }) {
  try {
    const response = await services.EventService.getEvent(id);
    return Promise.resolve(response.data);
  } catch (error) {
    return Promise.reject(error);
  }
}

export async function createEvent({ dispatch }, { data, services }) {
  try {
    const response = await services.EventService.createEvent(data);
    dispatch('addEvent', response.data)
    return Promise.resolve(response.data);
  } catch (error) {
    return Promise.reject(error);
  }
}

export async function deleteEvent({ dispatch }, { id, services }) {
  try {
    const response = await services.EventService.deleteEvent(id);
    console.log(response);
    dispatch('removeEvent', id)
    return Promise.resolve(true);
  } catch (error) {
    return Promise.reject(error);
  }
}

export const setEvents = ({ commit }, payload) => {
  commit("setEvents", payload.events);
  commit("setEventsPagination", payload.pagination);
};

export const addEvent = ({ commit }, payload) => {
  commit("addEvent", payload);
};

export const removeEvent = ({ commit }, payload) => {
  commit("removeEvent", payload);
};
