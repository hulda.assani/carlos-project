export const setEvents = (state, payload) => {
  state.events = payload || [];
};

export const addEvent = (state, payload) => {
  state.events.unshift(payload);
};

export const removeEvent = (state, payload) => {
  const index = state.events.findIndex((event) => event._id === payload);
  if (index !== -1) {
    state.events.splice(index, 1);
  }
};

export const setEventsPagination = (state, payload) => {
  state.eventsPagination = payload || null;
};
