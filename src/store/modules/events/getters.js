export const events = (state) => state.events;

export const getEventById = (state) => (id) => {
  return state.events.find((event) => event.id === id);
};

export const eventsPagination = (state) => state.eventsPagination;