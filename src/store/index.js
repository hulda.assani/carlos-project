import { createStore, createLogger } from "vuex";

import admin from "./modules/admin";
import events from "./modules/events";

const debug = process.env.NODE_ENV !== "production";

export default createStore({
  modules: {
    admin,
    events,
  },
  strict: debug,
  plugins: debug ? [createLogger()] : [],
});
