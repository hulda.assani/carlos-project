import { createApp } from "vue";
import App from "./App.vue";

import axios from "axios";
import VueAxios from "vue-axios";
import DatePicker from "vue3-datepicker";

import "bootstrap/dist/css/bootstrap.min.css";
import "bootstrap";
import "@/assets/css/style.css";

import store from "./store";
import router from "./router";
import date from "./configs/plugins/date";
import AuthService from "./services/auth";
import AdminService from "./services/admin";
import EventService from "./services/event";
import MediaFileService from "./services/media-file";

import { EVENT_MANAGER_SERVER_URL } from "./configs";

const app = createApp(App);

axios.interceptors.request.use(
  function (req) {
    req.headers = {
      "Content-Type": "application/json",
    };
    const authToken = AuthService(null).getToken();
    if (authToken) {
      req.headers.Authorization = `Bearer ${authToken.token}`;
    }
    return req;
  },
  function (error) {
    return Promise.reject(error);
  }
);
app.use(VueAxios, axios);
app.use(store);
app.use(router);

app.component("Datepicker", DatePicker);

app.config.globalProperties.$filters = {
  date,
};
app.config.globalProperties.$services = {
  AuthService: AuthService(axios),
  AdminService: AdminService(axios),
  EventService: EventService(axios),
  MediaFileService: MediaFileService(axios),
};
app.config.globalProperties.$config = {
  EVENT_MANAGER_SERVER_URL: EVENT_MANAGER_SERVER_URL,
};

app.mount("#app");
